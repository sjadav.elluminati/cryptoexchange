let mongoose = require('mongoose')
mongoose = require('../config/mongoose')
db = mongoose()

const initialData = async () => {
	const Admin = require('mongoose').model('admin')
	const adminData = require('./admin.json')
	const adminDetail = await Admin.findOne({})

	if (!adminDetail) {
		const admin = new Admin(adminData)
		await admin.save()
	}

	const Setting = require('mongoose').model('setting')
	const settingData = require('./settings.json')
	const settingDetail = await Setting.findOne({})

	if (!settingDetail) {
		const setting = new Setting(settingData)
		await setting.save()
	}
}
initialData()

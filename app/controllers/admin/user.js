require('../../utils/messageCode')
require('../../utils/errorCode')
require('../../utils/constant')
const utils = require('../../utils/utils')
const User = require('mongoose').model('user')

const ReferralCode = require('mongoose').model('referralCode')

exports.approveOrDeclineUser = async (req, res) => {
	try {
		utils.checkRequestParams(req.body, [{ name: 'userId', type: 'string' }])
		const requestDataBody = req.body
		const isApprove = requestDataBody.isApprove
		console.log(requestDataBody)
		if (requestDataBody.isApprove === 'true') {
			const user = await User.findOneAndUpdate({ _id: requestDataBody.userId }, { isApproved: true }, { new: true })
			if (!user) {
				throw ({ errorCode: COUNTRY_ERROR_CODE.UPDATE_FAILED })
			} else {
				// email to user approved --pending
				// sms to user approved --pending

				return res.json({ success: true, message: USER_MESSAGE_CODE.APPROVED_SUCCESSFULLY })
			}
		}
		if (isApprove === 'false') {
			const user = await User.findOneAndUpdate({ _id: requestDataBody.userId }, { isApproved: false }, { new: true })
			if (!user) {
				throw ({ errorCode: COUNTRY_ERROR_CODE.UPDATE_FAILED })
			} else {
				// email to user approved --pending
				// sms to user approved --pending

				return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.DECLINED_SUCCESSFULLY, true) })
			}
		}
	} catch (error) {
		console.log(error)
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.getUserReferralHistory = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		requestDataBody = req.body
		const condition = { $match: { userId: mongoose.Types.ObjectId(requestDataBody.id) } }
		const project = {
			$project: {
				firstName: '$userDetail.firstName',
				lastName: '$userDetail.lastName',
				phone: '$userDetail.phoneNumber',
				email: '$userDetail.email',
				currencySign: 1,
				createdAt: 1
			}
		}
		const lookup = {
			$lookup:
            {
            from: 'users',
            localField: 'referredId',
            foreignField: '_id',
            as: 'userDetail'
            }
		}
		const unwind = { $unwind: '$userDetail' }

		const referralDetail = await ReferralCode.aggregate([condition, lookup, unwind, project])

		if (!referralDetail) {
			throw ({ errorCode: ERROR_CODE.DETAILS_NOT_FOUND })
		}

		return res.json({ success: true, referralHistory: referralDetail })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

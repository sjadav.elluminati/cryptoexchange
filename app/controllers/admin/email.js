require('../../utils/errorCode')
require('../../utils/messageCode')
require('../../utils/constant')
let utils = require('../../utils/utils.js')
let Email = require('mongoose').model('emailDetail')
let Setting = require('mongoose').model('setting')

exports.emailList = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		const emailFinder = await Email.find()
		if (!emailFinder.length > 0) {
			throw ({ success: false, errorCode: utils.middleware(req.headers.lang, EMAIL_ERROR_CODE.CANT_GET_EMAIL_LIST, false) })
		}
		return res.json({ success: true, message: utils.middleware(req.headers.lang, EMAIL_MESSAGE_CODE.GET_EMAIL_LIST_SUCCESSFULLY, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, error: error.errorCode } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.emailDetail = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		const requestDataBody = req.body
		const emailId = requestDataBody.emailId
		console.log(emailId)
		const emailFinder = await Email.findById(emailId)
		if (!emailFinder) {
			throw ({ success: false, errorCode: utils.middleware(req.headers.lang, EMAIL_ERROR_CODE.CANT_GET_EMAIL, false) })
		}
		return res.json({
			success: true,
			message: utils.middleware(req.headers.lang, EMAIL_MESSAGE_CODE.GET_EMAIL_DETAIL_SUCCESSFULLY, true)
		})
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, error: error.errorCode } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.updateEmail = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		const requestDataBody = req.body
		const emailId = requestDataBody.emailId
		const emailFinderAndUpdate = await Email.findOneAndUpdate({ _id: emailId }, requestDataBody, { new: true })
		if (!emailFinderAndUpdate) {
			throw ({ success: false, errorCode: utils.middleware(req.headers.lang, EMAIL_ERROR_CODE.EMAIL_DETAIL_NOT_FOUND, false) })
		}
		return res.json({
			success: true,
			message: utils.middleware(req.headers.lang, EMAIL_MESSAGE_CODE.EMAIL_UPDATE_SUCCESSFULLY, true)
		})
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, error: error.errorCode } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

// email configuration get mail for whole App.
// exports.updateEmailConfiguration = async (req, res) => {
// 	try {
// 		await utils.checkRequestParams(req.body, [])
// 		const requestDataBody = requestData.body
// 		const settingFinder = await Setting.findOne({})
// 		if (!settingFinder) {
// 			throw ({ success: false, errorCode: utils.middleware(req.headers.lang, SETTING_ERROR_CODE.SETTING_DETAIL_NOT_FOUND, false) })
// 		}
// 		settingFinder.email = requestDataBody.email
// 		settingFinder.password = requestDataBody.password
// 		settingFinder.domain = requestDataBody.domain
// 		settingFinder.smtpPort = requestDataBody.smtpPort
// 		settingFinder.smtpHost = requestDataBody.smtpHost
// 		const settingSave = await settingFinder.save()
// 		if (!settingSave) {
// 			throw ({ success: false, errorCode: utils.middleware(req.headers.lang, SETTING_ERROR_CODE.SETTING_DETAIL_NOT_SAVED, false) })
// 		}

// 		// const emailUpdation = await Email.update({}, {  }, { new: true })
// 		if (!emailUpdation) {
// 			throw ({ success: false, errorCode: utils.middleware(req.headers.lang, EMAIL_ERROR_CODE.EMAIL_UPDATION_FAILED, false) })
// 		}
// 		return res.json({
// 			success: true,
// 			message: utils.middleware(req.headers.lang, EMAIL_MESSAGE_CODE.EMAIL_CONFIG_UPDATE_SUCCESSFULLY, true)
// 		})
// 	} catch (error) {
// 		if (error.errorCode || error.errorDescription) {
// 			const response = error.errorCode ? { success: false, error: error.errorCode } : { success: false, errorDescription: error.errorDescription }
// 			return res.json(response)
// 		} return res.json({
// 			success: false,
// 			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
// 		})
// 	}
// }

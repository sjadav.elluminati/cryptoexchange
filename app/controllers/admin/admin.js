require('../../utils/messageCode')
require('../../utils/errorCode')
require('../../utils/constant')
const utils = require('../../utils/utils')
const User = require('mongoose').model('user')
const Admin = require('mongoose').model('admin')

exports.addAdmin = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'adminName', type: 'string' }, { name: 'email', type: 'string' }, { name: 'password', type: 'string' }])
		const requestDataBody = req.body
		const password = requestDataBody.password

		const admin = await Admin.findOne({ email: ((requestDataBody.email).trim()).toLowerCase() })

		if (admin) {
			throw ({ errorCode: USER_ERROR_CODE.EMAIL_ALREADY_REGISTRED })
		}

		const newAdmin = new Admin({
			adminName: requestDataBody.adminName.trim().toLowerCase(),
			email: requestDataBody.email.trim().toLowerCase(),
			password: utils.encryptPassword(password)
		})

		const addedAdmin = await newAdmin.save()

		if (!addedAdmin) {
			throw ({ errorCode: ADMIN_ERROR_CODE.ADMIN_NOT_SAVED })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, ADMIN_MESSAGE_CODE.ADMIN_ADDED, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.adminList = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])

		const admins = await Admin.find({})

		if (!admins) {
			throw ({ errorCode: ADMIN_ERROR_CODE.DATA_NOT_FOUND })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, ADMIN_MESSAGE_CODE.ADMIN_LIST_SUCCESSFULLY, true), response: admins })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.updateAdmin = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'adminId', type: 'string' }])
		const requestDataBody = req.body

		if (requestDataBody.adminName) {
			const adminName = ((requestDataBody.adminName).trim()).toLowerCase()
			requestDataBody.adminName = adminName
		}

		if (requestDataBody.password !== '') {
			const password = requestDataBody.password
			requestDataBody.password = utils.encryptPassword(password)
		}

		if (requestDataBody.email) {
			const admin = await Admin.findOne({ _id: { $ne: requestDataBody.adminId }, email: ((requestDataBody.email).trim()).toLowerCase() })
			if (admin) {
				throw ({ errorCode: USER_ERROR_CODE.EMAIL_ALREADY_REGISTRED })
			}
		}

		const updatedAdmin = await Admin.findOneAndUpdate({ _id: requestDataBody.adminId }, requestDataBody, { new: true })

		if (!updatedAdmin) {
			throw ({ errorCode: COUNTRY_ERROR_CODE.UPDATE_FAILED })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, ADMIN_MESSAGE_CODE.UPDATED_SUCCESSFULLY, true) })
	} catch (error) {
		console.log(error)
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.getAdminDetail = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'adminId', type: 'string' }])
		const admin = await Admin.findById(req.body.adminId)

		if (!admin) {
			throw ({ errorCode: ADMIN_ERROR_CODE.DATA_NOT_FOUND })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, ADMIN_MESSAGE_CODE.GET_DETAIL_SUCCESSFULLY, true), response: admin })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.deleteAdmin = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'adminId', type: 'string' }])

		const admin = await Admin.remove({ _id: req.body.adminId })

		if (!admin) {
			throw ({ errorCode: ADMIN_ERROR_CODE.DATA_NOT_FOUND })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, ADMIN_MESSAGE_CODE.ADMIN_DELETED_SUCCESSFULLY, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.forgotPassword = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		const requestDataBody = req.body
		const type = Number(requestDataBody.type)
		switch (type) {
		case ADMIN_DATA_ID.ADMIN:
			Table = Admin
			string = 'admin'
			break
		case ADMIN_DATA_ID.USER:
			Table = User
			string = 'user'
			break
		default:
			break
		}

		if (requestDataBody.email && requestDataBody.email !== '') {
			// utils.verifyCaptcha() --pending

			const matchedUser = await Table.findOne({ email: requestDataBody.email })
			if (!matchedUser) {
				throw ({ errorCode: USER_ERROR_CODE.USERDATA_NOT_FOUND })
			}

			const otp = utils.generateOtp(6)
			// send via email --pending
			matchedUser.otp = otp
			const user = await matchedUser.save()

			if (!user) {
				throw ({ errorCode: ERROR_CODE.SOMETHING_WENT_WRONG })
			}

			return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.OTP_SET_SUCCESSFULLY, true) })
		}

		// send otp via mobile -----------
		// utils.verifyCaptcha --pending
		const matchedUser = await Table.findOne({ phoneNumber: requestDataBody.phoneNumber })

		if (!matchedUser) {
			throw ({ errorCode: USER_ERROR_CODE.USERDATA_NOT_FOUND })
		}

		const otp = utils.generateOtp(6)

		// send sms --pending
		matchedUser.otp = otp

		const user = await matchedUser.save()

		if (!user) {
			throw ({ errorCode: USER_ERROR_CODE.SOMETHING_WENT_WRONG })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.OTP_SET_SUCCESSFULLY, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

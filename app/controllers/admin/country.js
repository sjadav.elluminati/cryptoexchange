require('../../utils/messageCode')
require('../../utils/errorCode')
require('../../utils/constant')
const utils = require('../../utils/utils')

const Country = require('mongoose').model('country')

// add new country
exports.addNewCountry = async (req, res) => {
	try {
		const country = await Country.find({ countryPhoneCode: req.body.countryPhoneCode })
		await utils.checkRequestParams(req.body, [])
		if (country.length > 0) {
			throw ({ errorCode: COUNTRY_ERROR_CODE.COUNTRY_ALREADY_EXISTS })
		}
		const requestDataBody = req.body

		const countryName = utils.getStringWithFirstLetterUpperCase(requestDataBody.countryName)
		requestDataBody.countryName = countryName.trim()

		const fileNewName = (requestDataBody.countryCode).split(' ').join('_').toLowerCase() + '.png'
		const fileUploadPath = 'flags/' + fileNewName
		requestDataBody.countryFlag = fileUploadPath

		const addCountry = new Country(requestDataBody)

		const newCountry = await addCountry.save()
		if (!newCountry) {
			throw ({ errorCode: ERROR_CODE.SOMETHING_WENT_WRONG })
		}

		return res.json({
			success: true,
			message: utils.middleware(req.headers.lang, COUNTRY_MESSAGE_CODE.ADD_COUNTRY_SUCCESSFULLY, true)
		})
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

// get all business countries
exports.getCountryList = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		await Country.find({ isBusiness: true }).then(async (country) => {
			if (country.length === 0) {
				throw ({ errorCode: COUNTRY_ERROR_CODE.COUNTRY_DETAILS_NOT_FOUND })
			} else {
				return res.json({
					success: true,
					message: utils.middleware(req.headers.lang, COUNTRY_MESSAGE_CODE.COUNTRY_LIST_SUCCESSFULLY, true),
					countries: country
				})
			}
		})
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

// update country
exports.updateCountry = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'countryId', type: 'string' }])
		const requestDataBody = req.body
		const countryName = utils.getStringWithFirstLetterUpperCase(requestDataBody.countryName)
		requestDataBody.countryName = countryName.trim()

		const updatedCountry = await Country.findOneAndUpdate({ _id: requestDataBody.countryId }, requestDataBody, { new: true })
		if (!updatedCountry) {
			throw ({ errorCode: COUNTRY_ERROR_CODE.UPDATE_FAILED })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, COUNTRY_MESSAGE_CODE.UPDATED_SUCCESSFULLY, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

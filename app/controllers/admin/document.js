require('../../utils/messageCode')
require('../../utils/errorCode')
require('../../utils/constant')
const utils = require('../../utils/utils')
const User = require('mongoose').model('user')
const Document = require('mongoose').model('document')
const DocumentUploadedList = require('mongoose').model('documentUploadedList')

exports.addDocument = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'documentName', type: 'string' }])
		requestDataBody = req.body
		let documentName = (requestDataBody.documentName).trim()
		documentName = utils.getStringWithFirstLetterUpperCase(documentName)
		requestDataBody.documentName = documentName
		const document = new Document(requestDataBody)
		const users = await User.find({ countryId: document.countryId }).select({ _id: 1 }).lean()
		// console.log(users)
		const userDocuments = []
		users.forEach((user, index) => {
			userDocuments.push({
				userId: user._id,
				documentId: document._id,
				expiredDate: null,
				imageUrl: ''
			})
		})

		// const docList =
		await DocumentUploadedList.insertMany(userDocuments)

		if (document.isMandatory) {
			// const updatedUsers =
			await User.updateMany({ countryId: document.countryId }, { isDocumentUploaded: false }, { multi: true })
		}
		const newDoc = await document.save()
		if (!newDoc) {
			throw ({ errorCode: DOCUMENT_ERROR_CODE.FAILED_TO_ADD })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, DOCUMENT_MESSAGE_CODE.DOCUMENT_ADDED_SUCCESSFULLY, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.documentList = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		const countryQuery = {
			$lookup: {
				from: 'countries',
				localField: 'countryId',
				foreignField: '_id',
				as: 'countryDetails'
			}
		}
		const countryJson = { $unwind: '$countryDetails' }

		const documents = await Document.aggregate([countryQuery, countryJson])
		// const documents = await Document.find({ countryId: req.body.countryId })
		if (documents.length === 0) {
			throw ({ errorCode: DOCUMENT_ERROR_CODE.DOCUMENT_NOT_FOUND })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, DOCUMENT_MESSAGE_CODE.DOCUMENT_LIST_SUCCESSFULLY, true), response: documents })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.updateDocument = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])

		const requestDataBody = req.body
		const documentId = requestDataBody.documentId
		let documentName = (requestDataBody.documentName).trim()
		documentName = utils.getStringWithFirstLetterUpperCase(documentName)
		requestDataBody.documentName = documentName

		// console.log(req.body)
		// const matchedDocument = await Document.findOneAndUpdate({ _id: documentId }, requestDataBody, { new: true })

		const matchedDocument = await Document.findOne({ _id: documentId })
		matchedDocument.documentName = documentName
		matchedDocument.isMandatory = requestDataBody.isMandatory
		matchedDocument.isExpiredDate = requestDataBody.isExpiredDate

		const updatedDocument = await matchedDocument.save()

		if (!matchedDocument) {
			throw ({ errorCode: COUNTRY_ERROR_CODE.UPDATE_FAILED })
		}

		const users = await User.find({ countryId: matchedDocument.countryId })

		users.forEach(async (user) => {
			if (matchedDocument.isMandatory) {
				user.isDocumentUploaded = false
				await user.save()
			}
		})

		return res.json({ success: true, message: utils.middleware(req.headers.lang, DOCUMENT_MESSAGE_CODE.UPDATED_SUCCESSFULLY, true) })
	} catch (error) {
		console.log(error)
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.uploadDocument = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'documentId', type: 'string' }, { name: 'userId', type: 'string' }])
		const requestDataBody = req.body
		const document = await DocumentUploadedList.findOne({ documentId: requestDataBody.documentId, userId: requestDataBody.userId })

		if (!document) {
			throw ({ errorCode: ERROR_CODE.DETAILS_NOT_FOUND })
		}

		const imageFile = req.files
		if (imageFile !== undefined && imageFile.length > 0) {
			utils.deleteImageFromFolder(document.imageUrl, FOLDER_NAME.USER_DOCUMENTS)
			const imageName = document._id + utils.generateServerToken(4)
			const url = utils.getStoreImageFolderPath(FOLDER_NAME.USER_DOCUMENTS) + imageName + FILE_EXTENSION.USER
			document.imageUrl = url
			utils.storeImageToFolder(imageFile[0].path, imageName + FILE_EXTENSION.USER, FOLDER_NAME.USER_DOCUMENTS)
		}

		document.expiredDate = requestDataBody.expiredDate
		const updatedDocument = await document.save()

		if (!updatedDocument) {
			throw ({ errorCode: DOCUMENT_ERROR_CODE.FAILED_TO_UPLOAD })
		}

		const user = await User.findOne({ _id: updatedDocument.userId })

		if (!user) {
			throw ({ errorCode: USER_ERROR_CODE.USERDATA_NOT_FOUND })
		}

		user.isDocumentUploaded = true

		const updatedUser = await user.save()

		if (!updatedUser) {
			throw ({ errorCode: ERROR_CODE.SOMETHING_WENT_WRONG })
		}

		return res.json({ success: true, message: utils.middleware(req.headers.lang, DOCUMENT_MESSAGE_CODE.DOCUMENT_UPLOADED_SUCCESSFULLY, true) })
	} catch (error) {
		console.log(error)
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

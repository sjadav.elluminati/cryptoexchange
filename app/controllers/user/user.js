/* eslint-disable no-undef */
/* eslint-disable no-throw-literal */
// const mongoose = require('mongoose')

require('../../utils/messageCode')
require('../../utils/errorCode')
require('../../utils/constant')
const { use } = require('../../routes/admin')
const utils = require('../../utils/utils')
const User = require('mongoose').model('user')
const Country = require('mongoose').model('country')
const ReferralCode = require('mongoose').model('referralCode')
const DocumentUploadedList = require('mongoose').model('documentUploadedList')

// const userService = require('../../services/userService')

exports.userRegistration = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'email', type: 'string' }, { name: 'phoneNumber', type: 'string' }, { name: 'firstName', type: 'string' }])
		const requestDataBody = req.body
		let socialId = requestDataBody.socialId
		const socialIdArray = []

		if (socialId === undefined || socialId === null || socialId === '') {
			socialId = null
		} else {
			socialIdArray.push(socialId)
		}
		const country = await Country.findOne({ countryCode: requestDataBody.countryCode })
		let countryId = null
		if (country) {
			countryId = country._id
		}

		const alreadyRegisterUser = await User.findOne({ socialIds: { $all: socialIdArray } })

		if (alreadyRegisterUser) {
			throw ({ errorCode: utils.middleware(req.headers.lang, USER_ERROR_CODE.USER_ALREADY_REGISTER_WITH_SOCIAL, false) })
		}

		const newUser = await User.findOne({ email: requestDataBody.email })
		if (newUser) {
			if (socialId != null && newUser.socialIds.indexOf(socialId) < 0) {
				newUser.socialIds.push(socialId)
				const savedUser = await newUser.save()

				if (!savedUser) {
					throw ({ errorCode: utils.middleware(req.headers.lang, USER_ERROR_CODE.USER_NOT_SAVED, false) })
				}
				return res.json({
					success: true,
					message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.REGISTER_SUCCESSFULLY, true),
					user: newUser
				})
			}
		}

		const findUserByEmail = await User.findOne({ email: requestDataBody.email })
		if (findUserByEmail) {
			throw ({ errorCode: utils.middleware(req.headers.lang, USER_ERROR_CODE.EMAIL_ALREADY_REGISTRED, false) })
		}

		const findByPhone = await User.findOne({ countryPhoneCode: requestDataBody.countryPhoneCode, phoneNumber: requestDataBody.phoneNumber })
		if (findByPhone) {
			if (socialId != null && newUser.socialIds.indexOf(socialId) < 0) {
				newUser.socialIds.push(socialId)
				const savedUser = await newUser.save()

				if (!savedUser) {
					throw ({ errorCode: utils.middleware(req.headers.lang, USER_ERROR_CODE.USER_NOT_SAVED, false) })
				}
				return res.json({
					success: true,
					message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.REGISTER_SUCCESSFULLY, true),
					user: newUser
				})
			}
		}

		const findUserByPhone = await User.findOne({ phoneNumber: requestDataBody.phoneNumber })

		if (findUserByPhone) {
			throw ({
				errorCode: utils.middleware(
					req.headers.lang,
					USER_ERROR_CODE.PHONE_NUMBER_ALREADY_REGISTRED,
					false

				)
			})
		}

		const firstName = utils.getStringWithFirstLetterUpperCase(requestDataBody.firstName)
		const lastName = utils.getStringWithFirstLetterUpperCase(requestDataBody.lastName)

		const userData = new User({
			firstName,
			lastName,
			email: ((requestDataBody.email).trim()).toLowerCase(),
			password: requestDataBody.password,
			phoneNumber: requestDataBody.phoneNumber,
			socialIds: socialIdArray,
			loginBy: requestDataBody.loginBy,
			countryId,
			countryCode: requestDataBody.countryCode,
			address: requestDataBody.address,
			deviceType: requestDataBody.device_type,
			deviceToken: requestDataBody.deviceToken,
			isEmailVerified: requestDataBody.isEmailVerified,
			isPhoneNumberVerified: requestDataBody.isPhoneNumberVerified
		})
		userData.password = utils.encryptPassword(requestDataBody.password)
		const imageFile = req.files
		if (imageFile !== undefined && imageFile.length > 0) {
			const imageName = userData._id + utils.generateServerToken(4)
			const url = utils.getStoreImageFolderPath(FOLDER_NAME.USER_PROFILES) + imageName + FILE_EXTENSION.USER
			userData.imageUrl = url
			utils.storeImageToFolder(imageFile[0].path, imageName + FILE_EXTENSION.USER, FOLDER_NAME.USER_PROFILES)
		}

		const referralCodeString = utils.generateReferralCode(requestDataBody.countryCode, firstName, lastName)
		userData.referralCode = referralCodeString

		const findUserByReferral = await User.findOne({ referralCode: requestDataBody.referralCode })

		if (findUserByReferral && country) {
			const referralBonusToUser = country.referralBonusToUser
			const userRefferalCount = findUserByReferral.totalReferrals

			if (userRefferalCount < country.noOfUserUseReferral) {
				findUserByReferral.totalReferrals = +findUserByReferral.totalReferrals + 1
			}
			const userReferralCodeDetails = await findUserByReferral.save()
			if (!userReferralCodeDetails) {
				throw ({ errorCode: utils.middleware(req.headers.lang, USER_ERROR_CODE.USER_NOT_SAVED, false) })
			}

			userData.isReferral = true
			userData.referredBy = findUserByReferral._id

			const referralCode = new ReferralCode({
				userId: findUserByReferral._id,
				userUniqueId: findUserByReferral.uniqueId,
				userReferralCode: findUserByReferral.userReferralCode,
				referred_id: userData._id,
				referralBonusToUser
			})

			const referralCodeSave = referralCode.save()
			if (!referralCodeSave) {
				console.log('referralCodeSave')
				throw ({ errorCode: utils.middleware(req.headers.lang, USER_ERROR_CODE.REFERRAL_NOT_SAVED, false) })
			}
		}

		const savedUser = await userData.save()
		if (!savedUser) {
			console.log('saveduser')
			throw ({ errorCode: utils.middleware(req.headers.lang, USER_ERROR_CODE.USER_NOT_SAVED, false) })
		}
		return res.json({
			success: true,
			message: utils.middleware(
				req.headers.lang,
				USER_MESSAGE_CODE.REGISTER_SUCCESSFULLY,
				true
			)
		})
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			console.log(error.errorCode)
			const response = error.errorCode ? { success: false, error: error.errorCode } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.userLogin = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'email', type: 'string' }, { name: 'password', type: 'string' }])
		const requestDataBody = req.body
		// let matchedUser = await User.findOne({email: requestDataBody.email})
		let email = ((requestDataBody.email).trim()).toLowerCase()
		let phoneNumber = requestDataBody.phoneNumber
		let socialId = requestDataBody.socialId

		if (!email) {
			email = null
		}
		if (!phoneNumber) {
			phoneNumber = null
		}
		let encryptedPassword = requestDataBody.password
		if (socialId === undefined || socialId == null || socialId === '') {
			socialId = ''
		}
		if (encryptedPassword === undefined || encryptedPassword == null || encryptedPassword === '') {
			encryptedPassword = ''
		} else {
			encryptedPassword = utils.encryptPassword(encryptedPassword)
		}

		let query = {}
		if (requestDataBody.userLoginByEmail && requestDataBody.userLoginByPhone) {
			console.log('both')
			query = { $or: [{ email }, { phoneNumber }, { socialIds: { $all: [socialId] } }] }
		} else if (!requestDataBody.userLoginByEmail && requestDataBody.userLoginByPhone) {
			console.log('phone')
			query = { $or: [{ phoneNumber }, { socialIds: { $all: [socialId] } }] }
		} else if (requestDataBody.userLoginByEmail && !requestDataBody.userLoginByPhone) {
			console.log('email')
			query = { $or: [{ email }, { socialIds: { $all: [socialId] } }] }
		} else {
			console.log('else')
			query = { $or: [{ email }, { phoneNumber }, { socialIds: { $all: [socialId] } }] }
		}

		const matchedUser = await User.findOne(query)
		if (socialId === undefined || socialId == null || socialId === '') {
			socialId = null
		}

		if (!matchedUser) {
			throw ({ errorCode: ERROR_CODE.DETAILS_NOT_FOUND })
		}

		if (!matchedUser.isApproved) {
			throw ({ errorCode: USER_ERROR_CODE.NOT_APPROVED })
		}

		// ----------------------login by otp ----------------------------
		// let loginByOtp = false
		// loginByOtp = requestDataBody.loginByOtp

		// if (loginByOtp) {
		// 	const otp = utils.generateOtp(6)

		// }

		if (socialId == null && encryptedPassword !== '' && encryptedPassword !== matchedUser.password) {
			throw ({ errorCode: USER_ERROR_CODE.INVALID_PASSWORD })
		} else if (socialId != null && matchedUser.socialIds.indexOf(socialId) < 0) {
			throw ({ errorCode: USER_ERROR_CODE.USER_NOT_REGISTER_WITH_SOCIAL })
		} else {
			const jwtToken = utils.generateAuthToken(requestDataBody.email)
			matchedUser.jwtToken = jwtToken
			const loggedInUser = await matchedUser.save()

			if (!loggedInUser) {
				throw ({ errorCode: USER_ERROR_CODE.SOMETHING_WENT_WRONG })
			}

			return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.LOGGEDIN_SUCCESSFULLY, true) })
		}
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.userUpdate = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		const requestDataBody = req.body
		const userId = requestDataBody.userId
		let oldPassword = requestDataBody.oldPassword
		let socialId = requestDataBody.socialId
		let userEmailUpdate = false


		console.log(req.body)

		if (socialId === undefined || socialId == null || socialId === '') {
			socialId = null
		}

		if (oldPassword === undefined || oldPassword == null || oldPassword === '') {
			oldPassword = ''
		} else {
			oldPassword = utils.encryptPassword(oldPassword)
		}

		const matchedUser = await User.findOne({ _id: userId })
		if (!matchedUser) {
			throw ({ errorCode: USER_ERROR_CODE.USERDATA_NOT_FOUND })
		}

		if (requestDataBody.jwtToken !== null && matchedUser.jwtToken !== requestDataBody.jwtToken) {
			throw ({ errorCode: USER_ERROR_CODE.INVALID_AUTH_TOKEN })
		}

		if (socialId === null && oldPassword !== '' && oldPassword !== matchedUser.password) {
			throw ({ errorCode: USER_ERROR_CODE.INVALID_PASSWORD })
		}

		if (socialId !== null && matchedUser.socialIds.indexOf(socialId) < 0) {
			throw ({ errorCode: USER_ERROR_CODE.USER_ALREADY_REGISTER_WITH_SOCIAL })
		}

		const newEmail = requestDataBody.email
		const newPhone = requestDataBody.phoneNumber
		if (newEmail !== matchedUser.email) {
			userEmailUpdate = true
		}

		if (requestDataBody.newPassword !== '') {
			const newPassword = utils.encryptPassword(requestDataBody.newPassword)
			requestDataBody.password = newPassword
		}

		requestDataBody.socialIds = matchedUser.socialIds

		const userEmailDetail = await User.findOne({ _id: { $ne: userId }, email: newEmail })

		if (userEmailDetail) {
			throw ({ errorCode: USER_ERROR_CODE.EMAIL_ALREADY_REGISTRED })
		}

		// if (settingDetail.isUserEmailVerificationOn && (requestDataBody.isEmailVerified !== null || requestDataBody.isEmailVerified !== undefined)) {
		// 	matchedUser.isEmailVerified = false
		// }

		const userPhoneDetail = await User.findOne({ _id: { $ne: userId }, phoneNumber: newPhone })

		if (userPhoneDetail) {
			throw ({ errorCode: USER_ERROR_CODE.PHONE_NUMBER_ALREADY_REGISTRED })
		}

		// if (settingDetail.isUserPhoneVerificationOn && (requestDataBody.isPhoneNumberVerified !== null || requestDataBody.isPhoneNumberVerified !== undefined)) {
		// 	matchedUser.isPhoneNumberVerified = false
		// }

		const socialIdArray = []
		if (socialId !== null) {
			socialIdArray.push(socialId)
		}

		let userUpdateQuery = { $or: [{ password: oldPassword }, { socialIds: { $all: socialIdArray } }] }
		userUpdateQuery = { $and: [{ _id: userId }, userUpdateQuery] }

		const matechedUser = User.findOne({ _id: userId })

		const imageFile = req.files
		if (imageFile !== undefined && imageFile.length > 0) {
			utils.deleteImageFromFolder(matchedUser.imageUrl, FOLDER_NAME.USER_PROFILES)
			const imageName = matechedUser._id + utils.generateServerToken(4)
			const url = utils.getStoreImageFolderPath(FOLDER_NAME.USER_PROFILES) + imageName + FILE_EXTENSION.USER
			matechedUser.imageUrl = url
			utils.storeImageToFolder(imageFile[0].path, imageName + FILE_EXTENSION.USER, FOLDER_NAME.USER_PROFILES)
		}

		const firstName = utils.getStringWithFirstLetterUpperCase(requestDataBody.firstName)
		const lastName = utils.getStringWithFirstLetterUpperCase(requestDataBody.lastName)

		matechedUser.firstName = firstName
		matechedUser.lastName = lastName

		if (requestDataBody.isPhoneNumberVerified !== undefined) {
			matechedUser.isPhoneNumberVerified = requestDataBody.isPhoneNumberVerified
		}
		if (requestDataBody.isEmailVerified !== undefined) {
			matechedUser.isEmailVerified = requestDataBody.isEmailVerified
		}

		if (userEmailUpdate) {
			const jwtToken = utils.generateAuthToken(newEmail)
			matechedUser.email = newEmail
			matechedUser.jwtToken = jwtToken
		}

		const updatedUser = await User.findOneAndUpdate(userUpdateQuery, requestDataBody, { new: true })

		if (!updatedUser) {
			throw ({ errorCode: COUNTRY_ERROR_CODE.UPDATE_FAILED })
		}

		// const imageFile = req.files
		// if (imageFile !== undefined && imageFile.length > 0) {
		// 	utils.deleteImageFromFolder(matchedUser.imageUrl, FOLDER_NAME.USER_PROFILES)
		// 	const imageName = updatedUser._id + utils.generateServerToken(4)
		// 	const url = utils.getStoreImageFolderPath(FOLDER_NAME.USER_PROFILES) + imageName + FILE_EXTENSION.USER
		// 	updatedUser.imageUrl = url
		// 	utils.storeImageToFolder(imageFile[0].path, imageName + FILE_EXTENSION.USER, FOLDER_NAME.USER_PROFILES)
		// }

		// const firstName = utils.getStringWithFirstLetterUpperCase(requestDataBody.firstName)
		// const lastName = utils.getStringWithFirstLetterUpperCase(requestDataBody.lastName)

		// updatedUser.firstName = firstName
		// updatedUser.lastName = lastName

		// if (requestDataBody.isPhoneNumberVerified !== undefined) {
		// 	updatedUser.isPhoneNumberVerified = requestDataBody.isPhoneNumberVerified
		// }
		// if (requestDataBody.isEmailVerified !== undefined) {
		// 	updatedUser.isEmailVerified = requestDataBody.isEmailVerified
		// }

		// if (userEmailUpdate) {
		// 	const jwtToken = utils.generateAuthToken(newEmail)
		// 	updatedUser.email = newEmail
		// 	updatedUser.jwtToken = jwtToken
		// }

		// const user = await updatedUser.save()

		// if (!user) {
		// 	throw ({ errorCode: COUNTRY_ERROR_CODE.UPDATE_FAILED })
		// }

		return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.UPDATED_SUCCESSFULLY, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.logout = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [userId, jwtToken])
		const requestDataBody = req.body
		const user = await User.findById(requestDataBody.userId)
		console.log(user)
		if (!user) {
			throw ({ errorCode: USER_ERROR_CODE.USERDATA_NOT_FOUND })
		}
		if (requestDataBody.jwtToken !== null && requestDataBody.jwtToken !== user.jwtToken) {
			throw ({ errorCode: USER_ERROR_CODE.INVALID_AUTH_TOKEN })
		} else {
			user.jwtToken = ''
			user.deviceToken = ''

			const updatedUser = await user.save()
			if (!updatedUser) {
				throw ({ errorCode: USER_ERROR_CODE.SOMETHING_WENT_WRONG })
			}

			return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.LOGGEDOUT_SUCCESSFULLY, true) })
		}
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.userOtpVerification = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [{ name: 'userId', type: 'string' }])
		const requestDataBody = req.body
		const userId = requestDataBody.userId
		const matchedUser = await User.findOne({ _id: userId })

		if (!matchedUser) {
			throw ({ errorCode: USER_ERROR_CODE.USERDATA_NOT_FOUND})
		}

		if (requestDataBody.otp !== matchedUser.otp) {
			throw ({ errorCode: USER_ERROR_CODE.INVALID_OTP })
		}

		const updatedUser = await User.findOneAndUpdate({ _id: userId }, { otp: '' }, { new: true })

		return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.OTP_VERIFICATION_SUCCESSFULLY, true) })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

exports.deleteUser = async (req, res) => {
	try {
		await utils.middleware(req.body, [{ name: 'userId', type: 'string' }, { name: 'password', type: 'string' }])
		const requestDataBody = req.body
		const userId = requestDataBody.userId
		const password = requestDataBody.password

		const user = await User.findOne({ _id: userId })

		if (!user) {
			throw ({ errorCode: USER_ERROR_CODE.USERDATA_NOT_FOUND })
		}

		if (use.password !== utils.encryptPassword(password)) {
			throw ({ errorCode: USER_ERROR_CODE.INVALID_PASSWORD })
		}

		const documentDetail = await DocumentUploadedList.find({ userId }) 
		documentDetail.forEach(async (document) => {
			await document.remove({ _id: document._id })
		})

		console.log(documentDetail)

		await User.remove({ _id: userId })

		return res.json({ success: true, message: utils.middleware(req.headers.lang, USER_MESSAGE_CODE.USER_DELETED_SUCCESSFULLY, true) })
	} catch (error) {
		console.log(error)
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

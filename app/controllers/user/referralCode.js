require('../../utils/messageCode')
require('../../utils/errorCode')
require('../../utils/constant')
const utils = require('../../utils/utils')

const User = require('mongoose').model('user')
const Country = require('mongoose').model('country')

exports.checkReferral = async (req, res) => {
	try {
		await utils.checkRequestParams(req.body, [])
		const matchedUser = await User.find({ referralCode: req.body.userReferralCode })
		if (matchedUser.length === 0) {
			throw ({ errorCode: USER_ERROR_CODE.REFERRAL_NOT_FOUND })
		}
		const matchedUserCountry = await Country.findById(matchedUser[0].countryId)
		if (!matchedUserCountry.isReferralUser && matchedUserCountry.noOfUserUseReferral < matchedUser.totalReferrals) {
			throw ({ errorCode: USER_ERROR_CODE.REFERRAL_CODE_OUT_OF_USE })
		}

		return res.json({ success: true, message: USER_MESSAGE_CODE.VALID_REFERRAL })
	} catch (error) {
		if (error.errorCode || error.errorDescription) {
			const response = error.errorCode ? { success: false, errorCode: utils.middleware(req.headers.lang, error.errorCode, false) } : { success: false, errorDescription: error.errorDescription }
			return res.json(response)
		} return res.json({
			success: false,
			errorCode: utils.middleware(req.headers.lang, ERROR_CODE.SOMETHING_WENT_WRONG, false)
		})
	}
}

// exports.getReferralDetail = async (req, res) => {
//     await utils.checkRequestParams(req.body, []);
//     try {
//         let requestDataBody = req.body;
//         let details = await ReferralCode.aggregate([
//             {
//                 $lookup:
//                 {
//                     from: "users",
//                     localField: "referredId",
//                     foreignField: "_id",
//                     as: "userDetail"
//                 }
//             }
//         ])

//         if(details.length == 0){
//             throw ({errorCode: USER_ERROR_CODE.REFERRAL_DETAIL_NOT_FOUND})
//         }

//         return res.json({success: true, message:USER_MESSAGE_CODE.REFERRAL_DETAIL_LIST_SUCCESSFULLY})
//     } catch (error) {

//     }
// }

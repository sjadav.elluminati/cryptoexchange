const mongoose = require('mongoose')
// eslint-disable-next-line no-unused-vars
const User = mongoose.model('user')
const ReferralCode = mongoose.model('referralCode')
// eslint-disable-next-line no-unused-vars
const Country = mongoose.model('country')

const userService = {
	findUserByReference: async (user, country) => {
		let referralData
		const referralBonusToUser = country.referralBonusToUser
		const userReferralCount = user.totalReferrals

		if (userReferralCount < country.noOfUserUseReferral) {
			findUserByReferral.totalReferrals = +findUserByReferral.totalReferrals + 1
		}
		const userReferralCodeDetails = await findUserByReferral.save()
		if (!userReferralCodeDetails) {
			throw ({ errorCode: USER_ERROR_CODE.USER_NOT_SAVED })
		}

		referralData.isReferral = true
		referralData.referredBy = findUserByReferral._id

		const referralCode = new ReferralCode({
			userId: findUserByReferral._id,
			userUniqueId: findUserByReferral.uniqueId,
			userReferralCode: findUserByReferral.userReferralCode,
			referred_id: userData._id,
			referralBonusToUser
		})

		const referralCodeSave = referralCode.save()
		if (!referralCodeSave) {
			throw ({ errorCode: USER_ERROR_CODE.REFERRAL_NOT_SAVED })
		}
		return referralData
	}
}
module.exports = userService

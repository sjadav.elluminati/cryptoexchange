module.exports = FOLDER_NAME = {
	USER_PROFILES: 10,
	USER_DOCUMENTS: 11
}

module.exports = FILE_EXTENSION = {
	USER: '.jpg'
}

module.exports = ADMIN_DATA_ID = {
	USER: 1,
	ADMIN: 2
}

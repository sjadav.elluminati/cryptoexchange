const myUtils = require('./utils')
require('../utils/constant')
require('./errorCode')
const fs = require('fs')
// const http = require('hhtp')
const json = require('../utils/en.json')

exports.middleware = function (lang = 0, response, isSuccess) {
	try {
		const langIndex = lang > -1 ? lang : 0
		const langCode = 'en'
		// if (langIndex !== undefined && langIndex !== null) langCode = settingDetail.lang[langIndex].code

		// console.log(response)
		// console.log(json[langCode].successCode[response])
		// console.log(json)
		// console.log(isSuccess)
		// console.log(json.en.errorCode[602])
		// console.log(json[langCode].successCode[response])

		if (isSuccess) {
			json[langCode] !== undefined ? string = json[langCode].successCode[response] : string = json.en.successCode[response]
		} else {
			json[langCode] !== undefined ? string = json[langCode].errorCode[response] : string = json.en.errorCode[response]
			console.log(string)
		}
		return string
	} catch (error) {
		console.log(error)
	}
}

const jwt = require('jsonwebtoken')
exports.checkRequestParams = function (requestDataBody, paramsArray) {
	let missingParam = ''
	let isMissing = false
	let invalidParam = ''
	let isInvalidParam = false
	paramsArray.forEach(function (param) {
		if (requestDataBody[param.name] === undefined) {
			missingParam = param.name
			isMissing = true
		} else {
			// eslint-disable-next-line valid-typeof
			if (param.type && typeof requestDataBody[param.name] !== param.type) {
				isInvalidParam = true
				invalidParam = param.name
			}
		}
	})

	if (isMissing) {
		// console.log('missing_param: ' + missingParam)
		throw ({ errorDescription: missingParam + ' parameter missing' })
	} else if (isInvalidParam) {
		// console.log('invalid_param: ' + invalidParam)
		throw ({ errorDescription: missingParam + ' parameter invalid' })
	}
}

exports.getStringWithFirstLetterUpperCase = function (inputString) {
	let string = inputString
	try {
		if (string !== '' && string !== undefined && string != null) {
			string = inputString.trim()
			string = string.charAt(0).toUpperCase() + string.slice(1)
		} else {
			string = ''
		}
		return string
	} catch (error) {
		string = ''
		return string
	}
}

// For generating token
exports.generateServerToken = function (length) {
	try {
		if (typeof length === 'undefined') { length = 32 }
		let token = ''
		const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
		for (let i = 0; i < length; i++) { token += possible.charAt(Math.floor(Math.random() * possible.length)) }
		return token
	} catch (error) {
		console.error(error)
	}
}

exports.getStoreImageFolderPath = function (id) {
	return myUtils.getImageFolderName(id)
}

exports.getImageFolderName = function (id) {
	switch (id) {
	case FOLDER_NAME.USER_PROFILES:
		return 'userProfiles/'
	case FOLDER_NAME.USER_DOCUMENTS:
		return 'userDocuments/'
	default:
		break
	}
}

exports.getSaveImageFolderPath = function (id) {
	return './uploads/' + myUtils.getImageFolderName(id)
}

exports.storeImageToFolder = function (localImagePath, imageName, id) {
	// const fileNewPath = myUtils.getSaveImageFolderPath(id) + imageName
	exports.storeImageToFolder = function (localImagePath, imageName, id) {
		const fileNewPath = myUtils.getSaveImageFolderPath(id) + imageName
		// eslint-disable-next-line n/handle-callback-err
		fs.readFile(localImagePath, function (error, data) {
			fs.writeFile(fileNewPath, data, 'binary', function (error) {
				if (error) {
					console.log('Save file : ' + error)
				} else {
					if (fs.existsSync(localImagePath)) {
						fs.unlinkSync(localImagePath)
					}
				}
			})
		})
	}
}

exports.encryptPassword = function (password) {
	const crypto = require('crypto')
	try {
		return crypto.createHash('md5').update(password).digest('hex')
	} catch (error) {
		console.error(error)
	}
}

exports.generateReferralCode = function (countryCode = 'IN', firstName, lastName) {
	let referralCode = ''
	try {
		const length = 8
		const possible = (new Date()).getTime() + firstName.toUpperCase() + lastName.toUpperCase()

		for (let i = 0; i < length; i++) { referralCode += possible.charAt(Math.floor(Math.random() * possible.length)) }
	} catch (error) {
		referralCode = '' + (new Date()).getTime()
		referralCode = referralCode.substring(referralCode.length - 8, referralCode.length)
	}
	referralCode = countryCode.toUpperCase() + referralCode
	referralCode = referralCode.replace(/\s+/g, '')
	return referralCode
}

exports.generateAuthToken = function (email) {
	const payload = { subject: email }
	const token = jwt.sign(payload, 'CRYPTOEXCHANGE')
	return token
}

exports.deleteImageFromFolder = function (oldImage, id) {
	if (oldImage !== '' || oldImage !== null) {
		const oldFineName = oldImage.split('/')

		// let bf = new Buffer(100000);

		const oldFilePath = myUtils.getSaveImageFolderPath(id) + oldFineName[1]

		// if (settingDetail.is_use_aws_bucket) {
		// 	AWS.config.update({ accessKeyId: settingDetail.access_key_id, secretAccessKey: settingDetail.secret_key_id });
		// 	let s3 = new AWS.S3();
		// 	s3.deleteObject({
		// 		Bucket: settingDetail.aws_bucket_name,
		// 		Key: oldFilePath
		// 	}, function (err, data) { })
		// } else {

		if (fs.existsSync(oldFilePath)) {
			fs.unlink(oldFilePath, function (error, file) {
				if (error) {
					console.error(error)
				} else {
					console.log('successfully remove image')
				}
			})
		}
		// if (id === FOLDER_NAME.STORE_PRODUCTS || id === FOLDER_NAME.STORE_ITEMS || id === FOLDER_NAME.STORE_PROFILES || id === FOLDER_NAME.DELIVERY_TYPE_IMAGES) {
		// 	if (fs.existsSync(oldFilePath)) {
		// 		fs.unlink(oldFilePath.replace('.jpg', '_md.jpg').replace('.jpeg', '_md.jpeg').replace('.png', '_md.png'), function (error, file) {
		// 			if (error) {
		// 				console.error(error)
		// 			} else {
		// 				console.log('successfully remove image')
		// 			}
		// 		})
		// 		fs.unlink(oldFilePath.replace('.jpg', '_sm.jpg').replace('.jpeg', '_sm.jpeg').replace('.png', '_sm.png'), function (error, file) {
		// 			if (error) {
		// 				console.error(error)
		// 			} else {
		// 				console.log('successfully remove image')
		// 			}
		// 		})
		// 	}
		// }
		// }
	}
}

exports.generateOtp = function (length) {
	try {
		if (typeof length === 'undefined') {
			length = 32
		}
		let otpCode = ''
		const possible = '0123456789'

		for (let i = 0; i < length; i++) {
			otpCode += possible.charAt(Math.floor(Math.random() * possible.length))
		}
		return otpCode
	} catch (error) {
		console.log(error)
	}
}

exports.sendSMS = function (to, msg) {
	try {
		const accountSid = ''
		const authToken = ''
		const smsNumber = ''

		const client = require('twilio')(accountSid, authToken)

		client.messages.create({
			to,
			from: smsNumber,
			body: msg
		}).then((mes) => {
			console.log(mes)
		}).catch(error => {
			console.log(error)
		})
	} catch (error) {
		console.log(error)
	}
}

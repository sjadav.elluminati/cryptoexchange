const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose.connection)

const referralCode = new schema({
	uniqueId: Number,
	userId: { type: schema.Types.ObjectId },
	userUniqueId: Number,
	userReferralCode: { type: String, default: '' },
	referredId: { type: schema.Types.ObjectId },
	referralBonusToUser: { type: Number, default: 0 },
	createdAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	}

})

referralCode.plugin(autoIncrement.plugin, { model: 'referralCode', field: 'uniqueId', startAt: 1, incrementBy: 1 })
module.exports = mongoose.model('referralCode', referralCode)

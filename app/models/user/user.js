const mongoose = require('mongoose')
const schema = mongoose.Schema

const autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose.connection)

const user = schema({
	uniqueId: Number,
	firstName: { type: String, default: '' },
	lastName: { type: String, default: '' },
	email: { type: String, default: '' },
	password: { type: String, default: '' },
	phoneNumber: { type: String, default: '' },
	imageUrl: { type: String, default: '' },
	countryId: { type: schema.Types.ObjectId },
	countryPhoneCode: { type: String, default: '' },
	countryCode: { type: String, default: '' },
	loginBy: { type: String, default: '' },
	address: { type: String, default: '' },
	city: { type: String, default: '' },
	deviceToken: { type: String, default: '' },
	otp: { type: String, default: '' },
	referralCode: { type: String, default: '' },
	isReferral: { type: Boolean, default: false },
	socialId: { type: String, default: '' },
	socialIds: [{ type: String, default: '' }],
	referredBy: { type: schema.Types.ObjectId },
	totalReferrals: { type: Number, default: 0 },
	jwtToken: { type: String, default: '' },
	isApproved: { type: Boolean, default: false },
	isEmailVerified: { type: Boolean, default: false },
	// totalReferrals: { type: Boolean, default: false },
	isSetPin: { type: Boolean, default: false },
	isPhoneNumberVerified: { type: Boolean, default: false },
	isDocumentUploaded: { type: Boolean, default: true },
	createdAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	}
})

user.plugin(autoIncrement.plugin, { model: 'user', field: 'uniqueId', startAt: 1, incrementBy: 1 })
module.exports = mongoose.model('user', user)

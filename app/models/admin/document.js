const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose.connection)

const document = new schema({
	documentName: { type: String, default: '' },
	countryId: { type: schema.Types.ObjectId },
	isUniqueCode: { type: Boolean, default: false },
	isExpiredDate: { type: Boolean, default: false },
	isMandatory: { type: Boolean, default: false },
	isShow: { type: Boolean, default: false },
	createdAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	}

}, {
	strict: true,
	timestamps: {
		createdAt: 'createdAt',
		updatedAt: 'updatedAt'
	}
})

document.index({ countryId: 1 }, { background: true })
document.index({ isShow: 1 }, { background: true })
document.index({ isMandatory: 1 }, { background: true })

document.plugin(autoIncrement.plugin, { model: 'document', field: 'documentId', startAt: 1, incrementBy: 1 })
module.exports = mongoose.model('document', document)

const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose.connection)

const setting = new schema({
	lang: {
		type: Array,
		default: [{
			name: 'English',
			code: 'en',
			stringFilePath: '../app/utils/en.json'
		}]
	},
	created_at: {
		type: Date,
		default: Date.now
	},
	updated_at: {
		type: Date,
		default: Date.now
	},
	isUserEmailVerificationOn: { type: Boolean, default: false },
	isuserPhoneVerificationOn: { type: Boolean, default: false }
})

setting.plugin(autoIncrement.plugin, { model: 'setting', field: 'uniqueId', startAt: 1, incrementBy: 1 })
module.exports = mongoose.model('setting', setting)

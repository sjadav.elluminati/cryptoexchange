const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose.connection)

// eslint-disable-next-line new-cap
const country = new schema({
	uniqueId: Number,
	countryName: { type: String, default: '' },
	countryFlag: { type: String, default: '' },
	countryCode: { type: String, default: '' },
	currencyName: { type: String, default: '' },
	currencyCode: { type: String, default: '' },
	currencySign: { type: String, default: '' },
	countryPhoneCode: { type: String, default: '' },
	isBusiness: { type: Boolean, default: false },
	isReferralUser: { type: Boolean, default: true },
	noOfUserUseReferral: { type: Number, default: 0 },
	minimumPhoneNumberLength: { type: Number, default: 8 },
	maximumPhoneNumberLength: { type: Number, default: 10 },
	referralBonusToUser: { type: String, default: '' },
	createdAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	}
})

country.plugin(autoIncrement.plugin, { model: 'country', field: 'uniqueId', startAt: 1, incrementBy: 1 })
module.exports = mongoose.model('country', country)

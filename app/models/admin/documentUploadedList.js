const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment')
autoIncrement.initialize(mongoose.connection)

const documentUploadedList = new schema({
	documentId: { type: schema.Types.ObjectId },
	userId: { type: schema.Types.ObjectId },
	uniqueCode: { type: String, default: '' },
	imageUrl: { type: String, default: '' },
	isDocumentExpired: { type: Boolean, default: false },
	userTypeId: { type: schema.Types.ObjectId, default: null },
	expirationNotificationSentCount: { type: Number, default: 0 },

	expiredDate: {
		type: Date
	},
	createdAt: {
		type: Date,
		default: Date.now
	},
	updatedAt: {
		type: Date,
		default: Date.now
	}

}, {
	strict: true,
	timestamps: {
		createdAt: 'createdAt',
		updatedAt: 'updatedAt'
	}
})

documentUploadedList.index({ userId: 1 }, { background: true })

// document_uploaded_list.plugin(autoIncrement.plugin, {model: 'document_uploaded_list', field: 'unique_id', startAt: 1, incrementBy: 1});
module.exports = mongoose.model('documentUploadedList', documentUploadedList)

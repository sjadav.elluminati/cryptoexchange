const express = require('express')
const router = express.Router()

const user = require('../../controllers/user/user')
const referralCode = require('../../controllers/user/referralCode')

router.post('/api/user/registerUser', user.userRegistration)

router.post('/api/user/registerUser', user.userRegistration)
router.post('/api/user/checkReferral', referralCode.checkReferral)
router.post('/api/user/userLogin', user.userLogin)
router.post('/api/user/logout', user.logout)
router.post('/api/user/userUpdate', user.userUpdate)
router.post('/api/user/otpVerification', user.userOtpVerification)
router.post('/api/user/deleteUser', user.deleteUser)
// router.post('/api/user/sendSms', user.sendSms)

module.exports = router

require('dotenv').config()

let express = require('express')
let mongoose = require('mongoose')
// const ecosystem = require('./ecosystem.config')

settingDetails = {}

const port = process.env.PORT || 7000

const init = async () => {
	require('./config/config')
	express = require('./config/express')
	mongoose = require('./config/mongoose')
	mongoose()
	const app = express()

	const server = require('http').createServer(app)

	const Setting = require('mongoose').model('setting')
	const setting = await Setting.findOne({})
	settingDetails = setting

	server.listen(port, (error) => {
		if (error) {
			console.log(error)
		} else {
			console.log('server listen from port no. : ' + `${port}`)
		}
	})
	exports = module.exports = app
}

init()

module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es2021: true
	},
	extends: 'standard',
	overrides: [
	],
	parserOptions: {
		ecmaVersion: 'latest'
	},
	rules: {
		'no-shadow-restricted-names': 'error',
		'no-tabs': 0,
		'no-throw-literal': 'off',
		'no-undef': 'off',
		quotes: ['error', 'single'],
		indent: [1, 'tab'],
		semi: ['error', 'never']
	}
}

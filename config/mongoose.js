const config = require('./config')
const mongoose = require('mongoose')

module.exports = function () {
	let db
	if (config.db) {
		db = mongoose.connect(config.db, {
			useUnifiedTopology: true,
			useNewUrlParser: true
		})
	}

	require('../app/models/admin/admin')
	require('../app/models/admin/settings')
	require('../app/models/admin/country')
	require('../app/models/user/user')
	require('../app/models/user/referalCode')

	require('../app/models/admin/country')
	require('../app/models/user/user')
	require('../app/models/user/referalCode')
	require('../app/models/admin/settings')
	require('../app/models/admin/document')
	require('../app/models/admin/documentUploadedList')
	require('../app/models/email-sms/email')

	return db
}

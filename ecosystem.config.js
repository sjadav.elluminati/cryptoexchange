module.exports = {
	apps: [{
		name: 'backend',
		script: './server.js',
		env_production: {
			NODE_ENV: 'production',
			PORT: '7000',
			MONGODB_URL: 'mongodb://localhost:27017/cryptoExchange'
			// ACTIVITY_MONGODB_URL: "mongodb://localhost:27017/activity_log",
			// JWTPRIVATEKEY: "EDELIVERYELLUMINATIINC"
		},
		env_development: {
			NODE_ENV: 'development',
			PORT: '7000',
			MONGODB_URL: 'mongodb://localhost:27017/cryptoExchange'
			// ACTIVITY_MONGODB_URL: "mongodb://localhost:27017/activity_log",
			// JWTPRIVATEKEY: "EDELIVERYELLUMINATIINC"
		}
	}]
}
